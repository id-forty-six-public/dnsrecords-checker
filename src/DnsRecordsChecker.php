<?php

namespace idfortysix\dnsrecordschecker;

use Exception;

class DnsRecordsChecker
{
    private $errors = [];
    private $interfaces = [];
    private $app_ip;
    
    public function __construct()
    {
        $this->interfaces = function_exists('config') 
            ? config('services.dispatch.interfaces') 
            : json_decode($_ENV['INTERFACES'], true);
        
        if(isset($_ENV['APP_IP']))
        {
            $this->app_ip = $_ENV['APP_IP'];
        }
    }

    public function execute()
    {
        foreach ($this->interfaces as $interface) {
            $host = $interface['host'];
            $ip = $interface['ip'];
            $domain = $interface['domain'];

            $this->checkReverseDns($ip, $host);
            $this->checkMx($domain);
            $this->checkSpf($domain, $ip);
            $this->checkDkim($domain);

            // dispatchapi, mail, srcX A records
            $this->checkA($host, $ip);
            $this->checkA("mail.$domain", $ip);

            if (isset($this->app_ip))
            {
                $this->checkA("dispatchapi.$domain", $this->app_ip);
            }
        }

        if ($this->errors)
        {
            $errors = "WARNING - ";

            foreach ($this->errors as $err_msg) 
            {
                $errors.= "$err_msg ";
            }
            
            echo $errors . "\n";
        }
        else
        {
            echo "OK\n";
        }
        
    }

    private function checkReverseDns(string $ip, string $host)
    {
        try {
            $hostByAddr = gethostbyaddr( $ip );
        } catch(Exception $e) {
            $this->errors[] = '[' . "$ip reverse DNS record check failed. " . $e->getMessage() . ']';
        }

        if(!isset($hostByAddr) || $hostByAddr != $host)
        {
            $this->errors[] = '[' . "$ip reverse DNS record dont match $host" . ']';
        }
    }

    private function checkSpf(string $domain, string $ip)
    {
        try {
            $result = dns_get_record($domain, DNS_TXT);
        } catch(Exception $e) {
            $this->errors[] = '[' . "$domain SPF record check failed. " . $e->getMessage() . ']';
        }

        if(!isset($result[0]['txt']) || !strpos($result[0]['txt'], $this->getSpf($ip)) )
        {
            $this->errors[] = '[' . "$domain SPF record is incorrect" . ']';
        }
    }

    private function getSpf($ip) 
    {
        $temp = explode(".",$ip);
        
        $temp = array_slice($temp, 0, 3, true);
        
        $result = implode(".", $temp);
        
        return "ip4:" . $result . ".0/24";
    }

    private function checkMx($domain) 
    {
        try {
            $result = dns_get_record($domain, DNS_MX);
        } catch(Exception $e) {
            $this->errors[] = '[' . "$domain MX record check failed. " . $e->getMessage() . ']';
        }

        if(!isset($result[0]))
        {
            $this->errors[] = '[' . "$domain MX record is missing" . ']';
        }
    }

    private function checkA(string $host, string $ip)
    {
        try {
            $result = dns_get_record($host, DNS_A);
        } catch(Exception $e) {
            $this->errors[] = '[' . "$host A record check failed. " . $e->getMessage() . ']';
        }

        if(!isset($result[0]['ip']) || $result[0]['ip'] != $ip)
        {
            $this->errors[] = '[' . "$host A record is missing or incorrect" . ']';
        }
    }

    private function checkDkim(string $domain)
    {
        $dkim_selector = isset($_ENV['DKIM_SELECTOR']) ? $_ENV['DKIM_SELECTOR'] : 'sendapi2';
        $dkim_host = $dkim_selector.'._domainkey.'.$domain;

        $dkim_public_key = $this->getDkim();

        if($dkim_public_key)
        {
            try {
                $result = dns_get_record($dkim_host, DNS_TXT);
            } catch(Exception $e) {
                $this->errors[] = '[' . "$dkim_host DKIM record check failed. " . $e->getMessage() . ']';
            }

            if (!isset($result[0]['txt'])) 
            {
                $this->errors[] = '[' . "$dkim_host DKIM DNS_TXT record is missing" . ']';
            }
            elseif(isset($result[0]['txt']) && $result[0]['txt'] != $dkim_public_key)
            {
                $this->errors[] = '[' . "$dkim_host DKIM record dont match public key. " 
                . "DNS_DKIM: " . $result[0]['txt'] . " "
                . "LOCAL_DKIM: " . $dkim_public_key . ']';
            }
        }
        else
        {
            $this->errors[] = '[' . "DKIM public key not found" . ']';
        }
    }

    private function getDkim() 
    {
        $dkim_path = "/etc/opendkim/dkim.public";

        if (file_exists($dkim_path) && is_readable($dkim_path)) 
        {
            try
            {
                $dkim_file = fopen($dkim_path, "r");
                $dkim_contents = fread($dkim_file, filesize($dkim_path));
                fclose($dkim_file);

                $filtered = array_filter(preg_split("/(\r\n|\n|\r)/",$dkim_contents));
              
                return 'v=DKIM1; p=' . implode('', array_slice($filtered, 1,-1));
            }
            catch( Exception $e)
            {
                $this->errors[] = '[' . "$dkim_path". $e->getMessage() . ']'; 
            }
        }
    }
}
