## Install
```
./composer.phar require id-forty-six/dnsrecords-checker
```

## Usage
Checks server A, SPF, MX, DKIM records
```
(new DnsRecordsChecker)->execute()
```